# camera-data

One database to rule them all.

Well at least when it comes to support cameras and lenses.

Targetting Rawtherapee/darktable/exiv2. One script to update all the metadata
needed.

## Goals

### TL;DR:

Reduce time it takes for an user being able to use their new camera and lens
with our opensource tools.

### The slightly longer version

Right now all the different opensource raw libraries are tracking camera
support independently. Of course the information will be shared somehow
("Lets see if the others already have something for this camera") But this is
still not a really coordinated effort.

As a first step we will create a database which includes the same information
we have in the current data files and hardcoded in the source of Rawtherapee
and darktable. And then have an exporter that generates the old fileformats
again for integration. We will probably have to also incorporate exiv2 in this
effort, as the mapping of lensID to lens name is handled through exiv2.

In a later step one could work on having all data in data files and make them
runtime loadable. That way we could have a tool similar ```lensfun-update-data```
that downloads new copies of those data files to provide a fast turn around
time for users.

It might even be worth to exploring the idea of having a library that exposes
the data model to the other libraries so they don't have to handle the data
format themself.